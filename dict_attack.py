from string import maketrans
from collections import OrderedDict

def main():
    print("Enter a cirphertext word...")

    kbd = raw_input()
    try:
        ciphertext = str(kbd).rstrip()
    except ValueError:
        print("Invalid Input")

    word_list = open("words_alpha.txt", "r")

    print("looking for matches...")
    cipherPattern = findPattern(ciphertext)
    for word in word_list:
        wordPattern = findPattern(word.rstrip())
        if cipherPattern == wordPattern:
            print(word.rstrip())
    word_list.close()

# given a ciphertext word, returns pattern
def findPattern(ciphertext):
    #print(ciphertext)
    # str with English alphabet
    alphabet_str = "abcdefghijklmnopqrstuvwxyz"

    # create a list of all unique characters in the ciphertext
    # in order from left to right. convert to a string
    unique_list = list(OrderedDict.fromkeys(ciphertext))
    unique_str = ''.join(unique_list)
    #print(unique_str)

    # create the mapping sequence based on the size of the
    # cipher text. Create translation table
    mapping_pattern = alphabet_str[:len(unique_str)]
    #print(mapping_pattern)
    mapping_table = maketrans(unique_str, mapping_pattern)

    # translate and return pattern
    pattern = ciphertext.translate(mapping_table)
    #print(pattern)
    return pattern

if __name__ == "__main__":
    main()