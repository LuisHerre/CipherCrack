from collections import Counter

print("Starting Frequency Analysis")

doi_file = open("doi.txt","r")

data_template = {
    'a':0,
    'b':0,
    'c':0,
    'd':0,
    'e':0,
    'f':0,
    'g':0,
    'h':0,
    'i':0,
    'j':0,
    'k':0,
    'l':0,
    'm':0,
    'n':0,
    'o':0,
    'p':0,
    'q':0,
    'r':0,
    's':0,
    't':0,
    'u':0,
    'v':0,
    'w':0,
    'x':0,
    'y':0,
    'z':0
}

data = Counter(data_template)
for line in doi_file:
    data.update(line.lower())

x_axis = []
for key in data.keys():
    x_axis.append(key)

y_axis = []
for value in data.values():
    y_axis.append(value)

char_count = sum(y_axis)

print("Data: ")
print(data)
print(char_count)