Name: Luis Herrera


Word Patterns:


Ciphertext#1:

KCXCHUHCRIB KCGO RIKA CI RDW XCITB. QDH CP YO DBO RDW

CXUZCIUHCRIB, RDW JRBBCQCKCHCOB QOVRXO KCXCHKOBB.

—FUXCO JURKCIOHHC


because punctuation is present, I can tell its a quote.

I'll run my program against the longest word.


Input:

JRBBCQCKCHCOB

Output:

Enter a cirphertext word...
JRBBCQCKCHCOB
looking for matches...
possibilities

only one output. I'll start to construct the key from this.


limitations live only in our minds. but if we use our
KCXCHUHCRIB KCGO RIKA CI RDW XCITB. QDH CP YO DBO RDW

imaginations, our possibilities become limitless.
CXUZCIUHCRIB, RDW JRBBCQCKCHCOB QOVRXO KCXCHKOBB.

—jamie paolinetti
—FUXCO JURKCIOHHC


key:

ABCDEFGHIJKLMNOPQRSTUVWXYZ
YQVTOPZ CF KXIRJ WBHDGY A

it help to have punctuations and the word sizes. Going for the longest word yileds most of the key.


Ciphertext#2:

GLZT ND VDO ND GLCB VDO LZPC BOFCXYE EDNCM 

XZTLCX TLZB ZHILZRCTYE ?  

ZBWGCX : TLC WZFC TLYBJ . WYFIHC WORWTZTYDBW  

XCIHZEC DBC WVFRDH DU IHZYBTCQT GYTL DBC  

WVFRDH DU EYILCXTCQT .

Input:
WORWTZTYDBW

Output:
Enter a cirphertext word...
WORWTZTYDBW
looking for matches...
substations
tautologist
typtologist


GLZT ND VDO ND GLCB VDO LZPC BOFCXYE EDNCM
what do you do when you have numeric codes 

XZTLCX TLZB ZHILZRCTYE ?
rather than alphabetic ?  

ZBWGCX : TLC WZFC TLYBJ . WYFIHC WORWTZTYDBW
answer : the same thing . simple substations  

XCIHZEC DBC WVFRDH DU IHZYBTCQT GYTL DBC
replace one symbol of plaintext with one  

WVFRDH DU EYILCXTCQT .
symbol of ciphertext .


key:

ABCDEFGHIJKLMNOPQRSTUVWXYZ
 NEOCMWLPJ HSDUVXB TFYSRIA

 this cipher was more challenging because I had 3 different words from my program and
it made me doubt my initial guesses. Also, two characters map to S, so the cipher is
polyalphabetic, which threw of some guesses because I was thinking it would be monoalphabetic.


